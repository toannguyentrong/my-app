import React, { Component } from 'react'
import './App.scss'

const launchIntoFullscreen = () => {
  const element = document.documentElement
  if (element.requestFullscreen) {
    element.requestFullscreen()
  } else if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen()
  } else if (element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen()
  } else if (element.msRequestFullscreen) {
    element.msRequestFullscreen()
  }
}

const exitFullscreen = () => {
  if (document.exitFullscreen) {
    document.exitFullscreen()
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen()
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen()
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen()
  }
}
const isFullScreen = () => {
  console.log('callme')
  return document.fullscreen || document.msfullscreen
    || document.webkitIsFullScreen || document.mozFullScreen
    || (window.screen.width === window.innerWidth && window.screen.height === window.innerHeight)
}

const supportsFullScreen = () => document.exitFullscreen || document.mozCancelFullScreen ||
  document.webkitExitFullscreen || document.msExitFullscreen

class App extends Component {
  state = { time: 200, isFull: false }
  componentDidMount() {
    // window.addEventListener('resize', this.checkFullScreen)
    console.log(isFullScreen())
    document.addEventListener('webkitfullscreenchange', this.test, false)
    document.addEventListener('fullscreenchange', this.test, false)
    document.addEventListener('mozfullscreenchange', this.test, false)
    document.addEventListener('msfullscreenchange', this.test, false)
  }

  test = () => {
    const isFullScreen = document.fullscreen || document.mozFullScreen ||
      document.webkitIsFullScreen || document.msFullscreenElement || false
    this.setState({ isFull: isFullScreen })
  }

  checkFullScreen = () => {
    const result = isFullScreen()
    this.setState({ isFull: result })
    console.log('call me check', result)
  }

  componentWillMount() {
    window.removeEventListener('resize', this.checkFullScreen)
  }

  launch = () => {
    launchIntoFullscreen()
    this.checkFullScreen()
  }

  render() {
    return (
      <div>
        <h2>Testing app</h2>
        <button onClick={this.launch}>CLICK</button>
        <div className="situation">
          <h1>hello {this.state.isFull.toString()}</h1>
        </div>
      </div>
    )
  }
}

export default App
