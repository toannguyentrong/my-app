import React from 'react'
import './timer.scss'
import TimerNumber from './TimeNumber';

const convertTimetoPosition = time => {
  return {
    firstNumber: Math.floor(time / 60 / 10),
    secoundNumber: Math.floor(time / 60 % 10),
    thirdNumber: Math.floor(time % 60 / 10),
    lastNumber: Math.floor(time % 60 % 10)
  }
}

const Timer = ({ time }) => {
  const number = convertTimetoPosition(time)
  return (
    <div>
      <div className="timer--clock">
        <TimerNumber number={number.firstNumber} position="first" />
        <TimerNumber number={number.secoundNumber} position="secound" />
        <div className="separator">:</div>
        <TimerNumber number={number.thirdNumber} position="third" />
        <TimerNumber number={number.lastNumber} position="last" />
      </div>
    </div>
  )
}

export default Timer
