import React from 'react'
import './timer.scss'
import anime from 'animejs'

class TimerNumber extends React.Component {
  componentDidMount() {
    console.log('mount')
    this.spinEffect(this.props.number)
  }

  spinEffect = number => {
    anime({
      targets: `.${this.props.position} .spin`,
      translateY: [
        { value: -number * 50, duration: 600 },
      ],
      easing: 'easeOutExpo'
    })
  }
  componentWillReceiveProps(nextProps) {
    console.log('will')
    this.spinEffect(nextProps.number)
  }

  render() {
    return (
      <div className={`${this.props.position} number-grp`}>
        <div className="spin">
          <div className="num num-0">0</div>
          <div className="num num-1">1</div>
          <div className="num num-2">2</div>
          <div className="num num-3">3</div>
          <div className="num num-4">4</div>
          <div className="num num-5">5</div>
          <div className="num num-6">6</div>
          <div className="num num-7">7</div>
          <div className="num num-8">8</div>
          <div className="num num-9">9</div>
        </div>
      </div>
    )
  }
}

export default TimerNumber
