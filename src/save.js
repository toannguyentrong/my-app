toBlob = (data) => {
  var byteString = atob(data)
  var ab = new ArrayBuffer(byteString.length);

  // create a view into the buffer
  var ia = new Uint8Array(ab);

  // set the bytes of the buffer to the correct values
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var blob = new Blob([ab], { type: 'audio/wav' });
  return blob;
}

dataURItoBlob = (dataURI) => {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(',')[1])

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);

  // create a view into the buffer
  var ia = new Uint8Array(ab);

  // set the bytes of the buffer to the correct values
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var blob = new Blob([ab], { type: mimeString });
  return blob;
}

convert = (data) => {
  var mp3Data = []
  var mp3encoder = new lamejs.Mp3Encoder(1, 44100, 128); //mono 44.1khz encode to 128kbps
  var samples = data; //one second of silence replace that with your own samples
  var mp3Tmp = mp3encoder.encodeBuffer(samples); //encode mp3

  //Push encode buffer to mp3Data variable
  mp3Data.push(mp3Tmp);

  // Get end part of mp3
  mp3Tmp = mp3encoder.flush();

  // Write last data to the output data, too
  // mp3Data contains now the complete mp3Data
  mp3Data.push(mp3Tmp);
  return mp3Data

};

test = (blob) => {
  var arrayBuffer;
  var fileReader = new FileReader();

  fileReader.onload = () => {
    arrayBuffer = fileReader.result;

    var buffer = new Uint8Array(arrayBuffer)
    var data = this.convert(buffer);
    window.test = data
    var mp3Blob = new Blob([data[0]], { type: 'audio/mp3' });
    this.audio.src = URL.createObjectURL(mp3Blob)

  }

  fileReader.readAsArrayBuffer(blob);

}
