import React, { Component } from 'react'

class Group extends Component {
  componentDidMount() {

    const steps = [
      {
        title: 'Trigger Action',
        text: 'It can be `click` (default) or `hover` <i>(reverts to click on touch devices</i>.',
        selector: '.card',
        position: 'top',
      }
    ];

    this.props.addSteps(steps);
  }

  render() {
    return (
      <div className="group">
        Group
        <h2 className="card">this is header</h2>
      </div>
    )
  }
}

export default Group;
